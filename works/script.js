$(document).ready(function(){

	var images = <?php echo json_encode($itemImages)?>;
	var imageUnit = <?php echo $imgUnit?>;
	var maxDispImageNum = 10000;

	var $grid = $('.gallery');
	var $moreButton = $('button.more');
	var $loading = $('.loading');

	$grid.isotope({
		itemSelector: '.item',
		percentPosition: true,
		masonry: {
			columnWidth: '.item-sizer'
		},
		transitionDuration: 0
	});

	var processing = false;
	function appendItems(){
		if( processing ) return false;
		processing = true;

		// console.log('processing:'+processing);

		var len = imageUnit>images.length ? images.length : imageUnit;
		var $tag;
		for( var i=0; i<len; i++ ){
			var img = images[0];

			var tag = '';
			tag += '<div class="item">';
			tag += '<img src="'+ img.src +'" />';
			tag += '<div class="caption">'+ img.caption +'</div>';
			tag += '</div>';

			if( !$tag ) $tag = $(tag);
			else $tag = $tag.add( $(tag) );

			images.shift();
		}
		$grid.append( $tag ).isotope( 'appended', $tag );

		var restCount = len;
		$grid.imagesLoaded().progress( function() {
			// 画像の読み込み完了

			var remove_num = $grid.find('.item').length - maxDispImageNum;
			console.log('remove_num:'+ remove_num);
			if( remove_num > 0 ){
				var cnt=0;
				$grid.find('.item').each(function(){
					if( cnt == remove_num ) return false;
					$grid.isotope( 'remove', $(this) );
					// console.log('loadedImage:'+$grid.find('.item').length);
					cnt++;
				});
			}

			restCount--;
			if( restCount == 0 ){
				console.log('done');
				processing = false;
			}
			$grid.isotope('layout');
			
		});

		console.log('restImage:'+ images.length);
		if( images.length == 0 ){
			$loading.hide();
		}
	}

	function init(){
		appendItems();
		$grid.find('.item').eq(0).addClass('main');

		// var removing = false;
		// setInterval(function(){
		// 	if( removing ) return false;
		// 	removing = true;

		// 	var remove_num = $grid.find('.item').length - maxDispImageNum;
		// 	console.log('remove_num:'+ remove_num);
		// 	if( remove_num > 0 ){
		// 		var cnt=0;
		// 		$grid.find('.item').each(function(){
		// 			if( cnt == remove_num ) return false;

		// 			$grid.isotope( 'remove', $(this) );
		// 			console.log('loadedImage:'+$grid.find('.item').length);
		// 			cnt++;
		// 		});
		// 	}
		// 	removing = false;

		// }, 400);
	}
	init();

	$moreButton.bind('click',function(){
		appendItems();
	});

	var $activeItem = null;
	$(document).on( 'click', '.gallery .item', function(){
		if( $activeItem ){
			$activeItem.removeClass( 'active' );
			$activeItem.find('.caption').hide();
			$activeItem = null;
		}

		$activeItem = $(this);
		if( !$activeItem.hasClass('main') ){ // メイン画像でない
			$activeItem.addClass( 'active' );
		}
		
		$activeItem.find('.caption').show();
	
		$grid.isotope( 'layout' );
		
	});

	
	$(window).bind( 'scroll', function(){
		if( processing ) return false;

		// processing = true;
		var t = $(window).scrollTop();
		var h = $(window).height();
		var e = $loading.offset().top;

		// console.log('elm:' + e);
		// console.log('scrollTop:'+ t );
		// console.log('height:'+ h );

		if( e > t && e < t+h ){
			// 画面内にローディングが表示された
			appendItems();
		}
	});
});