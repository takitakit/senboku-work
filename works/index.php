<?php

$itemImages = [];

$imgUnit = 10;
$imgNum = 400;
for( $i=0; $i<$imgNum; $i++ ){
	$w = rand( 500, 1500 );
	$h = rand( 500, 1500 );

	$r = dechex(rand(0,240));
	if( strlen($r)==1 ) $r = '0'.$r;
	$g = dechex(rand(0,240));
	if( strlen($g)==1 ) $g = '0'.$g;

	$rgb = '';
	for($j=0;$j<3;$j++){
		$c = dechex(rand(0,240));
		if( strlen($c)==1 ) $c = '0'.$c;
		$rgb .= $c;
	}

	$url = "https://placehold.jp/{$rgb}/000000/{$w}x{$h}.png?text=%E3%83%80%E3%83%9F%E3%83%BC";

	$itemImages[] = [
		'width' => $w,
		'height' => $h,
		'caption' => 'ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト',
		'src' => $url,
	];
}

//共通
require_once substr($_SERVER['SCRIPT_FILENAME'], 0, -strlen($_SERVER['SCRIPT_NAME'])).'/common/includes/init.php';

//メタディスクリプション
$description = '';

//メタキーワード
$keywords = '';

//Facebook　全ページ共通の場合は空白にしてください
$fbimage = '';

//タイトル
$title = '';

//css追加
$ex_tag_css = '<link rel="stylesheet" href="css/style.css">';

//js追加
$ex_tag_js = '<script src="/common/js/lib/isotope.pkgd.min.js"></script><script src="/common/js/lib/imagesloaded.pkgd.min.js"></script>';
ob_start();
include( 'script.js' );
$ex_tag_js .= '<script>'. ob_get_contents(). '</script>';
ob_end_clean();

//bodyID追加
$bodyID = '';

?>
  <?php include 'header.php'; ?>

<style>
.loading{
	max-width: 1200px;
	width: 100%;
	position: relative;
	margin: 0 auto;
	text-align: center;
	background-color: #34495E;
}

.gallery{
	max-width: 1200px;
	width: 100%;
	position: relative;
	margin: 0 auto;
}

/* PC */
/* メイン画像（最初の1個） */
.gallery .item.main{
	width: 50%;
}
.gallery .item.active{
	width: 33.33%;
}
.gallery .item{
	position: relative;
	width: 16.67%;
	height: auto;
	border: 1px solid #fff;
	box-sizing: border-box;
}
.gallery .item-sizer{
	width: 16.67%;
	position: relative;
}

.gallery .item img{
	width: 100%;
	height: auto;
	position: relative;
}
.gallery .item .caption{
	display: none;
}

/* SP */
@media screen and (max-width: 767px){
	/* メイン画像（最初の1個） */
	.gallery .item.main{
		width: 100%;
	}
	.gallery .item.active{
		width: 100%;
	}
	.gallery .item{
		width: 50%;
	}
	.gallery .item-sizer{
		width: 50%;
	}
}

</style>


  <div id="contents">
    <div class="cmn-inner">

	<div class="gallery">
		<div class="item-sizer"></div>
		</div>
	</div>
	<div class="loading">
		<img src="/works/img/loading.gif" />
	</div>

    </div><!-- // .cmn-inner -->
  </div>
  <!--//#contents-->
  <?php include 'footer.php'; ?>
