<?php

$itemImages = [];

$imgUnit = 10;
$imgNum = 100;
for( $i=0; $i<$imgNum; $i++ ){
	$w = rand( 500, 1500 );
	$h = rand( 500, 1500 );

	$r = dechex(rand(0,240));
	if( strlen($r)==1 ) $r = '0'.$r;
	$g = dechex(rand(0,240));
	if( strlen($g)==1 ) $g = '0'.$g;

	$rgb = '';
	for($j=0;$j<3;$j++){
		$c = dechex(rand(0,240));
		if( strlen($c)==1 ) $c = '0'.$c;
		$rgb .= $c;
	}

	$url = "https://placehold.jp/{$rgb}/000000/{$w}x{$h}.png?text=%E3%83%80%E3%83%9F%E3%83%BC";
	// $url = "/works/img/dummy.png";

	$itemImages[] = [
		'width' => $w,
		'height' => $h,
		'y' => null,
		'x' => null,
		'id' => (string)($i+1),
		'text' => 'ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト',
		'src' => $url,
		'main' => $i==0 ? true : false,
	];
}

//共通
require_once substr($_SERVER['SCRIPT_FILENAME'], 0, -strlen($_SERVER['SCRIPT_NAME'])).'/common/includes/init.php';

//メタディスクリプション
$description = '';

//メタキーワード
$keywords = '';

//Facebook　全ページ共通の場合は空白にしてください
$fbimage = '';

//タイトル
$title = '';

//css追加
$ex_tag_css = '<link rel="stylesheet" href="css/style.css">';

//js追加
$ex_tag_js = '<script src="/common/js/lib/velocity/velocity.min.js"></script><script src="/common/js/lib/imagesloaded.pkgd.min.js"></script>';
ob_start();
include( 'script2.js' );
$ex_tag_js .= '<script>'. ob_get_contents(). '</script>';
ob_end_clean();

//bodyID追加
$bodyID = '';

?>
  <?php include 'header.php'; ?>

<style>
.cmn-inner{
	overflow: visible;
}
.overlay{
	position: fixed;
	top: 0;
	height: 0;
	width: 100%;
	height: 100%;
	z-index: 25;
	/*background-color: rgba(255,255,255,0.5);*/
	background-color: rgba(0,0,0,0.4);
	display: none;
}
.loading{
	width: 100%;
	position: relative;
	margin: 0 auto;
	text-align: center;
	background-color: #34495E;
}

.gallery{
	width: 100%;
	position: relative;
	margin: 0 auto;
	overflow: visible;
}

/* PC */
.gallery .item{
	position: absolute;
	/*width: 16.67%;*/
	height: auto;
	border: 1px solid #fff;
	box-sizing: border-box;
	opacity: 0;
	cursor: pointer;
	background-color: #fff;
	border-radius: 0;
}

.gallery .item img{
	width: 100%;
	height: auto;
	position: relative;
}
.gallery .item .caption{
	display: none;
}
.gallery .item .close{
	position: absolute;
	opacity: 0;
	top: 0;
	right: 0;
	width: 40px;
	height: 40px;
	background-color: #fff;
	z-index: 100;
}

</style>

	<div class="overlay"></div>
  <div id="contents">
    <div class="cmn-inner">

	<div class="gallery"></div>
	<div class="loading">
		<img src="/works/img/loading.gif" />
	</div>

    </div><!-- // .cmn-inner -->
  </div>
  <!--//#contents-->
  <?php include 'footer.php'; ?>
