$(document).ready(function(){

	var unPlacedImages = <?php echo json_encode($itemImages)?>;		// 未配置の画像※
	var placedImages = [];											// 配置済みの画像※
																	// 配置済み=配置のための計算済み(DOM上の配置ではない)
	var $grid = $('.gallery');
	var $loading = $( '.loading' );
	var $overlay = $('.overlay');

	var winHeight = 0;					// ブラウザ表示領域高さ
	var winWidth = 0;					// ブラウザ表示領域幅
	var scrollTop = 0;					// スクロール位置（画面上部）
	var contWidth = 0;					// フォトギャラリーコンテンツ要素幅
	var contTop = $grid.offset().top;	// フォトギャラリーコンテンツ要素の配置Y座標

	var gridMode = '';							// グリッド表示モード(PC/SP)
	var gridNumCol = {'PC': 6, 'SP': 2};		// グリッド列数
	var mainImageCol = {'PC': 3, 'SP': 2}		// メイン画像のグリッド占有列数
	var dispBaseWidth = {'PC':1180, 'SP':722};	// 基準とするグリッド全体の横幅
	var dispRatio = 1;							// 基準となるコンテンツ幅(dispBaseWidth)
												// に対する、現在のコンテンツ要素幅の割合
	var placeUnit = 20;							// 一度に配置する画像の数
	var restoreThreshold = 10;					// 画像を復元するときに、現在画面に表示されている画像に加えて上下それぞれどれだけ余分に残すか

	var SpWidth = 767;							// SPモードの上限幅

	var processing = false;
	var gridHeightMap = [];	// グリッドの各列ごとの高さ（下端Y座標）を管理する配列

	//
	// リサイズイベント
	//
	var prevContWidth = 0;
	$(window).bind( 'resize', function(){

		// ブラウザ表示領域、コンテンツ要素サイズの更新
		winHeight = window.innerHeight;
		winWidth = window.innerWidth;
		contWidth = $grid.width();

		// PC / SP モードの判定
		var mode = '';
		if( winWidth <= SpWidth ) mode = 'SP';
		else mode = 'PC';

		if( gridMode != mode ){
			// モードの切り替え検知
			gridMode = mode;
			init();
			return;
		}

		if( contWidth != prevContWidth ){
			// コンテンツ幅が変化した

			// 基準となるコンテンツ幅(dispBaseWidth)に対する、現在のコンテンツ要素幅の割合を計算する
			dispRatio = contWidth / dispBaseWidth[gridMode];
			// _d('dispRatio:'+dispRatio);

			// DOM上の要素の位置調整を行う
			$grid.find('.item').each(function(){

				// 要素の、基準となるx,y座標、および幅を取得
				var x = $(this).attr('data-x');
				var y = $(this).attr('data-y');
				var w = $(this).attr('data-w');

				// 実際の配置位置を計算・設定
				var ax = x * dispRatio;
				var ay = y * dispRatio;
				var aw = Math.round( w * dispRatio );

				$(this).velocity({
					translateX: ax+'px',
					translateY: ay+'px',
					width: aw+'px'
				},0);
			});

			// コンテンツ要素の高さを更新
			$grid.css( 'height', Math.max.apply(null,gridHeightMap)*dispRatio );

			activateImage();
			activating = false;

		}
		prevContWidth = contWidth;

		onDispChanged();
	} );
	
	//
	// スクロールイベント
	//
	$(window).bind( 'scroll', function(){
		scrollTop = $(window).scrollTop();

		onDispChanged();
	});

	//
	// 表示領域の変化時
	//
	function onDispChanged(){

		// 処理中ならスキップ
		if( processing ) return false;
		processing = true;
	
		var t = $loading.offset().top;
		if( unPlacedImages.length>0 && scrollTop<=t && t<=scrollTop+winHeight ){
			// 画面内にローディングが表示された(画像の配置がすべて完了していない場合)

			// 画像の追加
			placeItems()
				.done(function() {
					// 画像の再配置
					restoreItems();
				})
				.done(function(){
					processing = false; // 処理フラグOFF
				});

			if( unPlacedImages.length == 0 ){
				// すべての画像の配置が完了した
				// ローディングを非表示
				$loading.hide();
			}
		}else{
			// 

			// 画像の再配置
			restoreItems()
				.done(function(){
					processing = false;
				});
		}
		
	}

	//
	// 画像の配置
	// 
	function placeItems(){
		var placed = 0;
		var $tags;
		while( unPlacedImages.length>0 && placed<placeUnit ){

			// 画像を先頭から1枚取り出し
			var img = unPlacedImages.splice(0,1)[0];

			// 画像の占有列数を決定
			if( img.main ) img.col = mainImageCol[gridMode]; // メイン画像
			else img.col = 1; // その他
			// console.log(img);

			// 画像を配置する列インデックスをチェックする（最も下端のY座標が小さい）
			var min=999999;
			var min_idx = -1;
			for( var k in gridHeightMap ){
				if( min > gridHeightMap[k] ){
					min = gridHeightMap[k];
					min_idx = parseInt(k);
				}
			}

			// 画像を配置する位置、サイズを計算(基準コンテンツ幅を基準に)
			var y = gridHeightMap[min_idx];
			var x = dispBaseWidth[gridMode] / gridNumCol[gridMode] * min_idx;
			var w = dispBaseWidth[gridMode] * (img.col/gridNumCol[gridMode]);
			var h = (w/img.width) * img.height;

			img.x = x;
			img.y = y;
			img.w = w;

			// 現在のコンテンツ幅を基準に、実際の配置サイズ、位置を計算
			var ay = dispRatio * y;
			var ax = dispRatio * x;
			var aw = Math.round( dispRatio * w );

			// 各列の下端Y座標値を更新
			for( var i=min_idx; i<min_idx+img.col; i++ ){
				gridHeightMap[i] = y + h;
			}

			// グリッド全体の高さを設定
			$grid.css( 'height', Math.max.apply(null,gridHeightMap)*dispRatio );

			// タグの生成
			// data-x: 基準X座標 data-y: 基準Y座標 data-w: 基準画像幅
			var tag = '<div class="item" data-x="'+img.x+'" data-y="'+img.y+'" data-w="'+img.w+'">';
			tag += '<img src="'+ img.src +'" />';
			tag += '<div class="caption">'+ img.text +'</div>';
			tag += '</div>';

			var $tag = $(tag);
			if( !$tags ) $tags = $tag;
			else $tags = $tags.add( $tag );

			$tag.velocity( {translateX:ax+'px',translateY:ay+'px',width:aw+'px'},0 );
			img.$item = $tag;

			placedImages.push( img );	// 配置済みの画像として記録

			// console.log('x:'+x + ' y:'+ y + ' w:' + w + ' h:'+h );
			// console.log(gridHeightMap);
			// console.log('contHeight:'+max);
			// console.log()

			placed++;
		}

		var d = new $.Deferred;
		if( $tags ){
			// 画像をDOMに追加
			$grid.append( $tags );

			// 画像のロードイベント
			$grid.imagesLoaded()
				.progress(function(ins,img){
					// 画像の読み込み完了(1画像ずつ)
					$(img.img).closest('.item').show().velocity({opacity:1},500);
				})
				.always(function(){
					// 全画像の読み込み処理完了(失敗含む)
					_d('imageLoaded always(place)');
					// _d(placedImages);
					d.resolve();
				});
		}else{
			d.resolve();
		}
		return d.promise();
	}

	//
	// 配置済みの画像を復元する
	// dir=スクロールの向き 1:下方向 2:上方向
	//
	function restoreItems(){
		// _d( 'restoreItems:'+dir );

		// 上方向から、表示領域上（まだ配置されていないものを含む）の位置取得
		var idx_top = findDispImageIndex( 1 ) - restoreThreshold;
		var idx_bottom = findDispImageIndex( -1 ) + restoreThreshold;
		if( idx_top<0 ) idx_top = 0;
		if( idx_bottom>placedImages.length-1 ) idx_bottom = placedImages.length-1;

		// _d('resore border (top,bottom)=('+idx_top+','+idx_bottom+')');
		// _d('remove from 0 to ' + (idx_top-1));
		// _d('remove from '+idx_bottom+' to ' + (placedImages.length-1));
		// _d('add from '+idx_top+ ' to '+ idx_bottom);

		// 不要な要素をDOMから削除
		for( var i=idx_top-1; i>=0; i-- ){
			// _d('remove idx:'+i);
			var img = placedImages[i];
			if( img.$item ){
				img.$item.remove();
				img.$item = null;
			}
		}
		for( var i=idx_bottom+1; i<=placedImages.length-1; i++ ){
			// _d('remove idx:'+i);
			var img = placedImages[i];
			if( img.$item ){
				img.$item.remove();
				img.$item = null;
			}
		}

		// 必要であれば要素をDOMに追加
		var $tags;
		for( var i=idx_top; i<idx_bottom; i++ ){
			if( i<0 || i>placedImages.length-1 ) continue;

			var img = placedImages[i];
			if( img.$item ) continue; // DOMに配置済み

			// 保存している基準コンテンツを基準にした位置、サイズから、
			// 実際の値を計算する
			var ax = img.x * dispRatio;
			var ay = img.y * dispRatio;
			var aw = Math.round( img.w * dispRatio );

			// タグの生成
			var tag = '<div class="item" data-x="'+img.x+'" data-y="'+img.y+'" data-w="'+img.w+'">';
			tag += '<img src="'+ img.src +'" />';
			tag += '<div class="caption">'+ img.text +'</div>';
			tag += '</div>';

			var $tag = $(tag);
			if( !$tags ) $tags = $tag;
			else $tags = $tags.add( $tag );

			$tag.velocity( {translateX:ax+'px',translateY:ay+'px',width:aw+'px'},0 );

			img.$item = $tag;
		}

		var d = new $.Deferred;
		if( $tags ){
			$grid.append( $tags );

			$grid.imagesLoaded()
				.progress(function(ins,img){
					// 画像の読み込み完了(1画像ずつ)
					$(img.img).closest('.item').show().velocity({opacity:1},500);
				})
				.always(function(){
					// 画像の読み込み処理完了(失敗含む)
					_d('imageLoaded always(restore)');
					// _d(placedImages);
					d.resolve();
				});
		}else{
			d.resolve();
		}
		return d.promise();
	}

	//
	// 画面に表示されている画像を取得する
	// (最初にマッチした画像)
	// dir=走査する方向 1:上から下 -1:下から上
	//
	function findDispImageIndex( dir ){
		var st = scrollTop - $grid.offset().top;

		// 配置済みの画像を走査し、画面上に表示されている画像を検出する
		var s = dir>0 ? 0 : placedImages.length-1;
		var e = dir>0 ? placedImages.length-1 : 0;
		var i = s;
		while( true ){
			if( (dir>0&&i>e) || (dir<0&&i<e) ) break;

			var img = placedImages[i];

			var y = img.y * dispRatio;
			var h = img.height * dispRatio;

			if( ( y<st && st<y+h ) || 
				( y<st+winHeight && st+winHeight<y+h ) ||
				( st<y && y+h<st+winHeight ) ){
				// 表示領域に含まれる画像を発見
				// (一部含む)
				return i;
			}

			i += dir;
		}
		return null;
	}

	//
	// 画像のクリックイベント
	//
	var $activeImage = null;
	var activating = false;
	$(document).on('click','.gallery .item', function(){
		// クリックされたのが、現在アクティブな画像であればスキップ
		if( $activeImage && $activeImage.get(0)==$(this).get(0) ) return false;

		// 処理フラグON
		if( activating ) return false;
		activating = true;

		activateImage( null );		// 現在アクティブな画像を解除
		activateImage( $(this) ) 	// クリックされた画像をアクティブに
			.done(function(){
				activating = false;
			});
		return false;
	});

	//
	// オーバーレイのクリックイベント
	//
	$overlay.bind('click',function(){
		if( activating ) return false;
		activating = true;

		activateImage( null )
			.done(function(){
				activating = false;
			});
		return false;
	});

	function activateImage( $elm ){
		var d = new $.Deferred;
		if( $elm ){
			// 有効にする
			_d('activate');

			// 背景オーバーレイを表示
			$overlay
				.velocity('stop')
				.velocity('fadeIn',200);

			// DOMに配置されている状態の位置、サイズを保存しておく
			var matrix = $elm.css( 'transform' );
			var orig_x=0, orig_y=0, orig_w=0, orig_h=0;
			if( matrix.match(/matrix\s*\((.+)\s*,\s*(.+)\s*,\s*(.+)\s*,\s*(.+)\s*,\s*(.+)\s*,\s*(.+)\s*\)/) ){
				orig_x = RegExp.$5;
				orig_y = RegExp.$6;
			}
			orig_w = $elm.css('width');
			orig_h = $elm.css('height');

			var st = scrollTop - contTop;
			var w = gridMode=='PC' ? contWidth*0.6 : contWidth;
			var h = ( w / parseInt(orig_w) ) * parseInt(orig_h);
			var x = ( contWidth - w ) / 2;
			var y = ( winHeight - h ) / 2 + st;

			$elm
				.attr( 'data-orig-x', orig_x+'px' )
				.attr( 'data-orig-y', orig_y+'px' )
				.attr( 'data-orig-w', orig_w )
				.css( {zIndex:50} )
				.velocity( 'stop' )
				.velocity( 
					{
						translateX: x+'px', 
						translateY: y+'px', 
						width: w+'px', 
						padding: '10px',
						borderRadius: '5px'
					}, 300,
					function(){
						// 閉じるボタンを追加
						var $close = $('<div class="close"></div>');
						$elm.append( $close );
						$close.velocity( 'fadeIn', 200 );

						// 説明文を表示
						$elm.find( '.caption' ).velocity( 'slideDown', 200, function(){
							d.resolve();
						});
					}
				);
			$activeImage = $elm;
		}else{
			// 無効にする

			// オーバーレイを非表示にする
			$overlay
				.velocity('stop')
				.velocity('fadeOut',200);

			if( $activeImage ){
				_d('deactivate');

				// アクティブ前の要素の位置、サイズを取得
				var orig_x = $activeImage.attr( 'data-orig-x' );
				var orig_y = $activeImage.attr( 'data-orig-y' );
				var orig_w = $activeImage.attr( 'data-orig-w' );
				// _d({translateX: orig_x, translateY: orig_y, width: orig_w});

				var $elm = $activeImage;
				$elm.find( '.caption' ).velocity( 'slideUp', 200 );		// 説明分を非表示に
				$elm.find( '.close' ).velocity( 'fadeOut', 100 );		// 閉じるボタンを非表示に
				$elm
					.removeAttr( 'data-orig-x' )
					.removeAttr( 'data-orig-y' )
					.removeAttr( 'data-orig-w' )
					.velocity( 'stop' )
					.velocity(
						{
							translateX: orig_x, 
							translateY: orig_y, 
							width: orig_w, 
							padding: 0, 
							borderRadius: 0
						}, 300, 
						function(){
							$elm.css( {zIndex:1} );
							$elm.find('.close').remove();	// 閉じるボタンを削除
							d.resolve();
						}
					);
				$activeImage = null;
			}
		}
		return d.promise();
	}

	//
	// 閉じるボタンのクリックイベント
	//
	$(document).on( 'click', '.gallery .item .close', function(){
		$overlay.trigger( 'click' );
		return false;
	} );

	//
	// 初期化処理
	//
	function init(){
		// 処理フラグのOFF
		processing = false;
		activating = false;

		$activeImage = null;

		// スクロール位置を先頭に戻す
		window.scrollTo( 0,0 );

		contTop = $grid.offset().top;

		$overlay.velocity( 'fadeOut',0 );

		// 表示モード判定
		if( window.innerWidth <= SpWidth ) gridMode = 'SP';
		else gridMode = 'PC';

		// 変数の初期化
		gridHeightMap = [];
		for( var i=0; i<gridNumCol[gridMode]; i++ ){
			gridHeightMap.push( 0 ); 
		}

		// すべて未配置状態に戻す
		if( placedImages.length ){
			// unPlacedImages.splice( 0, 0, placedImages );
			Array.prototype.push.apply( placedImages, unPlacedImages );
			unPlacedImages = placedImages;
			placedImages = [];
		}

		// 追加済みの画像要素を全削除
		$grid.find('.item').remove();

		// ローディングを再表示
		$loading.show();

		// リサイズ、スクロールイベントを強制的に発火する
		$(window).trigger( 'resize' );
		$(window).trigger( 'scroll' );

		// $grid.find('.item').eq(0).addClass('main');
	}
	init();

	function _d( msg ){
		console.log( msg );
	}
});