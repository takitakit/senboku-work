<?php
// インクルードパス追加
set_include_path(get_include_path().PATH_SEPARATOR.dirname(__FILE__));

function e($value, $default = '', $add_flag = false) {
  if (!empty($value)) {
    if ($add_flag && !empty($default)) {
      echo $value.' | '.$default;
    } else {
      echo $value;
    }
  } else {
    echo $default;
  }
}
