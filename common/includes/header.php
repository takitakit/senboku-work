<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <?php
$ua=$_SERVER['HTTP_USER_AGENT'];
if((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)){
echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
}else{
echo '<meta name="viewport" content="width=1200">';
}
?>
    <meta name="format-detection" content="telephone=no,address=no,email=no">
    <meta name="description" content="<?php e(@$description, 'ディスクリプションが入ります。') ?>">
    <meta name="keywords" content="<?php e(@$keywords, 'キーワード1,キーワード2') ?>">
    <!--Facebook -->
    <?php if ($_SERVER['REQUEST_URI'] == '/'): ?>
    <meta property="og:type" content="website">
    <?php else: ?>
    <meta property="og:type" content="article">
    <?php endif; ?>
    <meta property="og:title" content="<?php e(@$title, 'ここにサイト名が入ります', true) ?>">
    <meta property="og:url" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].htmlspecialchars($_SERVER['REQUEST_URI'],ENT_QUOTES); ?>">
    <meta property="og:image" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].(!empty($fbimage) ? $fbimage : '/common/img/thumb_fb.png'); ?>">
    <meta property="og:description" content="<?php e(@$description, 'ディスクリプションが入ります。') ?>">
    <meta property="og:site_name" content="ここにサイト名が入ります">
    <!--end -->
    <link href="https://fonts.googleapis.com/css?family=Pragati+Narrow:400,700" rel="stylesheet">
    <link rel="icon" href="/common/img/favicon.ico">
    <link rel="apple-touch-icon" href="/common/img/icon.png">
    <link rel="stylesheet" href="/common/css/common.css">
    <?php e(@$ex_tag_css) ?>
    <title>
      <?php e(@$title, 'ここにタイトルが入ります', true) ?>
    </title>
    <script type="text/javascript" src="//webfont.fontplus.jp/accessor/script/fontplus.js?AoLijhqMriI%3D&aa=1&ab=2" charset="utf-8"></script>
</head>
<body<?php if (!empty($bodyID)) echo ' id="'.$bodyID. '"'; ?>>
  <!-- ////////////////////////////// header start -->
  <header id="head">
    <div class="head-menu fbox">
      <?php if ($_SERVER['REQUEST_URI'] == '/'): ?>
      <h1 id="head-logo" class="fl"><a href="/"><img src="/common/img/logo.png" alt="SENBOKU HOME" width="274" height="45"></a></h1>
      <?php else: ?>
      <p id="head-logo" class="fl">
        <a href="/"><img src="/common/img/logo.png" alt="SENBOKU HOME" width="274" height="45"></a>
      </p>
      <?php endif; ?>
      <div class="head-menu-contents fr">
        <div class="head-menu-tel">
          <p class="cap-tel">お電話で問い合わせ</p>
          <!-- // .cap-tel -->
          <p class="txt-tel"><img src="/common/img/txt_tel.png" alt="0120-36-6668" width="158" height="22"></p>
          <!-- // .txt-tel -->
          <p class="txt-open">10:00 〜 18:00（火曜のみ17:00）</p>
          <!-- // .txt-open -->
        </div>
        <!-- // .head-menu-tel -->
        <div class="head-menu-counseling">
          <p class="cap-counseling">土地探し・資金相談も無料</p>
          <!-- // .cap-counseling -->
          <p class="btn-cmn btn-contact"><a href="sample"><span>WEBからお問い合わせ</span></a></p>
          <!-- // .btn-cmn -->
        </div>
        <!-- // .head-menu-counseling -->
        <div class="head-menu-catalog">
          <div class="btn-catalog pc-item">
            <a href="sample"><span>無料</span>資料請求</a>
          </div>
          <!-- // .btn-catalog -->
        </div>
        <!-- // .head-menu-catalog -->
      </div>
    </div>
    <nav id="head-nav">
      <ul>
        <li><a href="sample"><span>選ばれる理由</span></a></li>
        <li><a href="sample"><span>泉北ホームの注文住宅</span></a></li>
        <li><a href="sample"><span>家づくりの流れ</span></a></li>
        <li><a href="sample"><span>参考モデルプラン</span></a></li>
        <li><a href="sample"><span>建築実例</span></a></li>
        <li><a href="sample"><span>住生活研究ラボ</span></a></li>
        <li class="active"><a href="sample"><span>ショールーム</span></a></li>
        <li><a href="sample"><span>企業情報</span></a></li>
      </ul>
    </nav>
    <!-- <div id="sp-head-menu" class="sp-item">
      <div class="head-menu-bar"> <span class="bar"></span> <span class="bar"></span> <span class="bar"></span> </div>
      <span class="label label-menu">MENU</span>
      <span class="label label-close">CLOSE</span>
    </div> -->
  </header>
  <!-- ////////////////////////////// header end -->
  <div id="wrapper">
