<!-- ////////////////////////////// footer start -->
<footer id="foot" class="pc-item">
  <div class="foot-inner cmn-inner">
    <div class="foot-sct-catalog">
      <div class="foot-sct-catalog-contents">
        <h2 class="foot-sct-catalog-ttl">資料請求もお気軽に</h2>
        <!-- // .foot-sct-catalog-ttl -->
        <p class="foot-sct-catalog-txt">もっと泉北ホームの家づくりについて知りたい方、
          <br> 商品ごとにカタログを用意しております。
        </p>
        <!-- // .foot-sct-catalog-txt -->
      </div>
      <!-- // .foot-sct-catalog-contents -->
      <div class="foot-sct-catalog-btn btn-cmn"><a href="sample"><span>資料請求（無料）</span></a></div>
      <!-- // .foot-sct-catalog-btn -->
    </div>
    <!-- // .foot-sct-catalog -->
    <div class="foot-sct-contact">
      <div class="foot-sct-contact-from">
        <h3 class="foot-sct-contact-sttl">お問い合わせ・来場予約フォーム</h3>
        <!-- // .foot-sct-contact-sttl -->
        <ul class="foot-sct-contact-from-btn fbox">
          <li class="btn-cmn"><a href="sample"><span>土地探し依頼</span></a></li>
          <li class="btn-cmn"><a href="sample"><span>資金計画・間取り相談</span></a></li>
          <li class="btn-cmn"><a href="sample"><span>お問い合わせ</span></a></li>
        </ul>
        <!-- // .foot-sct-contact-from-btn -->
      </div>
      <!-- // .foot-sct-contact-from -->
      <div class="foot-sct-contact-tel">
        <h3 class="foot-sct-contact-sttl">お電話でのお問い合わせ</h3>
        <!-- // .foot-sct-contact-sttl -->
        <p class="foot-sct-contact-tel-txt"><img src="/common/img/txt_tel_foot.png" alt="0120-36-6668" width="271" height="31"></p>
        <!-- // .foot-sct-contact-tel-txt -->
      </div>
      <!-- // .foot-sct-contact-tel -->
    </div>
    <!-- // .foot-sct-contact -->
    <div class="wrap-foot-nav">
      <nav class="foot-nav">
        <h3 class="foot-nav-ttl"><a href="sample"><span>選ばれる理由</span></a></h3>
        <ul>
          <li><a href="sample"><span>家づくりの考え方</span></a></li>
          <li><a href="sample"><span>自社一貫施工</span></a></li>
          <li><a href="sample"><span>フル装備</span></a></li>
        </ul>
      </nav>
      <!-- // .foot-nav -->
      <nav class="foot-nav">
        <h3 class="foot-nav-ttl"><a href="sample"><span>泉北ホームの注文住宅</span></a></h3>
        <ul>
          <li><a href="sample"><span>設計・デザイン</span></a></li>
          <li><a href="sample"><span>設備仕様</span></a></li>
          <li><a href="sample"><span>構造</span></a></li>
          <li><a href="sample"><span>施工・品質保証</span></a></li>
        </ul>
      </nav>
      <!-- // .foot-nav -->
      <nav class="foot-nav">
        <h3 class="foot-nav-ttl"><a href="sample"><span>家づくりの流れ</span></a></h3>
        <ul>
          <li><a href="sample"><span>ご契約前</span></a></li>
          <li><a href="sample"><span>ご契約後</span></a></li>
          <li><a href="sample"><span>引渡し後</span></a></li>
        </ul>
      </nav>
      <!-- // .foot-nav -->
      <nav class="foot-nav">
        <h3 class="foot-nav-ttl"><a href="sample"><span>参考モデルプラン</span></a></h3>
        <ul>
          <li><a href="sample"><span>敷地面積別</span></a></li>
          <li><a href="sample"><span>こだわり別</span></a></li>
          <li><a href="sample"><span>デザイン別</span></a></li>
        </ul>
      </nav>
      <!-- // .foot-nav -->
      <nav class="foot-nav">
        <h3 class="foot-nav-ttl"><a href="sample"><span>建築実例</span></a></h3>
        <ul>
          <li><a href="sample"><span>建築実例一覧</span></a></li>
          <li><a href="sample"><span>お客様の声</span></a></li>
        </ul>
      </nav>
      <!-- // .foot-nav -->
      <nav class="foot-nav">
        <h3 class="foot-nav-ttl"><a href="sample"><span>住生活研究ラボ</span></a></h3>
        <ul>
          <li><a href="sample"><span>サブコンテンツタイトル</span></a></li>
          <li><a href="sample"><span>サブコンテンツタイトル</span></a></li>
          <li><a href="sample"><span>サブコンテンツタイトル</span></a></li>
        </ul>
      </nav>
      <!-- // .foot-nav -->
      <nav class="foot-nav">
        <h3 class="foot-nav-ttl"><a href="sample"><span>ショールーム</span></a></h3>
        <ul>
          <li><a href="sample"><span>家づくりギャラリー</span></a></li>
          <li><a href="sample"><span>モデルハウス</span></a></li>
        </ul>
      </nav>
      <!-- // .foot-nav -->
      <nav class="foot-nav">
        <h3 class="foot-nav-ttl"><a href="sample"><span>会社情報</span></a></h3>
        <ul>
          <li><a href="sample"><span>社長ごあいさつ</span></a></li>
          <li><a href="sample"><span>会社概要</span></a></li>
          <li><a href="sample"><span>沿革</span></a></li>
          <li><a href="sample"><span>スタッフ紹介</span></a></li>
          <li><a href="sample"><span>採用情報</span></a></li>
          <li><a href="sample"><span>採用エントリーフォーム</span></a></li>
        </ul>
      </nav>
      <!-- // .foot-nav -->
    </div>
    <!-- // .wrap-foot-nav -->
    <p class="foot-logo text-c"><img src="/common/img/logo_foot.png" alt="みんなの声で、できた家。SENBOKU HOME" width="199" height="43"></p>
    <!-- // .foot-logo -->
    <ul class="foot-utility text-c">
      <li><a href="sample"><span>お知らせ</span></a></li>
      <li><a href="sample"><span>イベント・キャンペーン</span></a></li>
      <li><a href="sample"><span>個人情報保護方針</span></a></li>
      <li><a href="sample"><span>サイトポリシー</span></a></li>
    </ul>
    <p class="foot-copy text-c">Copyright © 2001 - 2017 SENBOKU HOME All Rights Reserved.</p>
    <!-- // .foot-copy -->
  </div>
</footer>
<nav class="fix-nav">
  <ul class="fbox">
    <li>
      <a href="sample">
        <span class="fix-nav-img"><img src="/common/img/icon_fixnav01.png" alt="" width="31" height="31"></span>
        <span class="fix-nav-txt">ショールーム<br>
来場予約</span>
      </a>
    </li>
    <li>
      <a href="sample">
        <span class="fix-nav-img"><img src="/common/img/icon_fixnav02.png" alt="" width="26" height="31"></span>
        <span class="fix-nav-txt">カタログ<br>
無料請求</span>
      </a>
    </li>
    <li>
      <a href="sample">
        <span class="fix-nav-img"><img src="/common/img/icon_fixnav03.png" alt="" width="31" height="31"></span>
        <span class="fix-nav-txt">WEBから<br>
問い合わせ</span>
      </a>
    </li>
    <li>
      <a href="sample">
        <span class="fix-nav-img"><img src="/common/img/icon_fixnav04.png" alt="" width="31" height="31"></span>
        <span class="fix-nav-txt">お電話で<br>
問い合わせ</span>
      </a>
    </li>
  </ul>
</nav>
<!-- // .fix-nav sp-item -->
<!-- ////////////////////////////// footer end -->
<!--//#wrapper-->
</div>
<script src="/common/js/lib/jquery-2.1.4.min.js"></script>
<script src="/common/js/app_rsp.js"></script>
<script src="/common/js/common.js"></script>
<?php e(@$ex_tag_js) ?>
</body>

</html>
